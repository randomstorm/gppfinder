===========
GppFinder
===========

Description:

This script is used to gather the cpassword used in Group Policy Preferences and decrypt it.


Usage: 
  GPPfinder.py (-s <server>) [-b] [--poc]

  GPPfinder.py (-s <server>) [(-d <domain>) (-u <username>) (-p <password>)] [-b] [--poc]

  GPPfinder.py -h | --help

  GPPfinder.py --version



Options:
    --poc              If enabled passwords are obfuscated.
    -d <domain>        Target Domain name.
    -u <username>      A valid domain user account, username.
    -p <password>      A Valid domain user account, password.  
    -h --help          Show this screen.
    -b --debug         Turn on Debugging.
    --version          Show version.



Install Instructions
====================
GppFinder requires various other dependencies. So to make things as easy as possible I have
created the setup file to install the relevant dependents during the install. For the install
to work correctly please install using 'easy_install'.


Note: Installing Gppfinder using easy_install will install all dependants and allow the execution of the tool from any command line.

You can find instructions for installing easy_install/setuptools from here https://pypi.python.org/pypi/setuptools.


1.Mac and Unix users can simply use the following command; 

>sudo curl https://bootstrap.pypa.io/ez_setup.py -o - | python


2.Once the setuptools/easy_install, install is completed you are able to download the distribution from https://bitbucket.org/randomstorm/gppfinder/downloads/GppFinder-0.3.0.tar.gz and run the install/setup script using easy_install; 

>sudo easy_install GppFinder-0.3.0.tar.gz

3.You should now be able to execute the script from any working dir in a terminal (may need to close and reopen if in the same terminal as the one used to run the setup initially). 

>GppFinder.py