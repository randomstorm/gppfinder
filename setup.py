from distutils.core import setup

setup(
    name='GppFinder',
    version='0.2.4',
    author='Darryl lane',
    author_email='DarrylLane101@gmail.com',
    packages=['GppFinder', 'GppFinder'],
    url='https://bitbucket.org/laned/gppfinder',
    license='LICENSE.txt',
    description='Used to gather Group Policy Preferences credentials, decrypt them. Find other clear text credentials on NETLOGON/SYSVOL.',
    long_description=open('README.txt').read(),
    install_requires=[
        "docopt",
        "lxml",
        "pycrypto",
    ],
)

